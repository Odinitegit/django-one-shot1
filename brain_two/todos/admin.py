from django.contrib import admin
from .models import TodoItem,TodoList

# Register your models here.

class TodoListAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')

class TodoItemAdmin(admin.ModelAdmin):
    list_display = ('task', 'due_date', 'completed')

admin.site.register(TodoItem, TodoItemAdmin)
admin.site.register(TodoList, TodoListAdmin)







